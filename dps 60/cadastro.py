from pessoa import Pessoa
import re

def maior(pessoa: Pessoa):
    return pessoa.idade >= 18

def homem(pessoa: Pessoa):
    return pessoa.sexo == 'M'

def mul20(pessoa: Pessoa):
    return not homem(pessoa) and pessoa.idade < 20

pessoas = []
while(True):
    idade = int(input("Digite a idade da pessoa: "))
    sexo = input("Digite o sexo da pessoa (M/F): ").upper()
    ver = re.search('^[MF]{1}$', sexo)
    while(not ver):
        sexo = input("Digite o sexo da pessoa (M/F): ").upper()
        ver = re.search('^[MF]{1}$', sexo)
    pessoa = Pessoa(idade, sexo)
    pessoas.append(pessoa)
    cont = input("Continuar? (S/N) ").upper()  
    if(re.search('^[^S]$', cont)):
        break

maiores = 0
homens = 0
muls20 = 0

for i in range (len(pessoas)):
    if(maior(pessoas[i])): maiores += 1
    if(homem(pessoas[i])): homens += 1
    if(mul20(pessoas[i])): muls20 += 1

print("Exitem: \n{} pessoas maiores de idade,\n{} homens cadastrados,\n{} mulheres com menos de 20 anos de idade".format(maiores, homens, muls20))