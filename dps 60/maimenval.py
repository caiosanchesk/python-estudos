import re
soma = lambda n : n[0] if len(n) == 1 else n[0] + soma(n[1:])
numeros = []
def media(numeros):
    return round(soma(numeros) / len(numeros), 2)

def maior(numeros: list):
    numeros.sort(reverse=True)
    return numeros[0]

def menor(numeros: list):
    numeros.sort()
    return numeros[0]

while(True):
    numero = int(input("Digite um número: "))
    cont = input("Continuar (S/N): ").upper()
    while(not re.search("^[SN]$", cont)):
        print("SEU ANIMAL!")
        cont = input("Continuar (S/N): ").upper()
    numeros.append(numero)
    if(cont == 'N'):
        break
print(numeros)
print("Média: {}".format(media(numeros)))
print("Maior: {}".format(maior(numeros)))
print("Menor: {}".format(menor(numeros)))