import random as rd

def jogo(numero, escolha):
    computador = rd.randint(0,10)
    soma = numero + computador
    res = parImpar(soma)
    if(res == 'PAR'):
        return [escolha == 'P', computador, soma, res]
    return [escolha == 'I', computador, soma, res]

def parImpar(soma):
    return 'PAR' if soma % 2 == 0 else 'IMPAR'

qtd_vencidas = 0
while(True):
    valor = int(input("Escolha um valor: "))
    pI = input("(P)ar ou (I)mpar: ").upper()
    venceu = jogo(valor, pI)
    if(venceu[0]):
        print("Jogador jogou {}\nComputador jogou {}\nA soma é {} e um número {}"
              "\nPARABÉNS! VOCÊ VENCEU!\nVamos jogar de novo! ".format(valor, venceu[1], venceu[2], venceu[3]))
        qtd_vencidas += 1
    else:
        print("Jogador jogou {}\nComputador jogou {}\nA soma é {} e um número {}"
              "\nSEU BURRO ".format(valor, venceu[1], venceu[2], venceu[3]))
        print("Você venceu {} vezes".format(qtd_vencidas))
        break