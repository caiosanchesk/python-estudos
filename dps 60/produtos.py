from produto import Produto
import re

def total(produtos: list[Produto]):
    soma = 0
    for i in range(len(produtos)):
        soma += produtos[i].preco
    return soma

def mil(produto: Produto):
    return produto.preco > 1000

def qtdMil(produtos: list[Produto]):
    qtd = 0
    for i in range(len(produtos)):
        if(mil(produtos[i])): qtd += 1
    return qtd

def informacoes(produtos):
    print("O total gasto com a compra é: R$ {:.2f}".format(total(produtos)))
    print("Existem {} produtos com preço acima de R$ 1000.00".format(qtdMil(produtos)))
    produto_menor = menor(produtos)
    print("O produto de menor preço é: {} e seu preço é: R$ {:.2f}".format(produto_menor.nome, produto_menor.preco))

menor = lambda m: m[0] if len(m) == 1 or m[0].preco < menor(m[1:]).preco else menor(m[1:])
# [1,2,5,7,0,2] -> len() == 1 não, 1 < 0 não, retorna 0
# [2,5,7,0,2] -> len() == 1 não, 2 < 0 não, retorna 0
# [5,7,0,2] -> len() == 1 não, 5 < 0 não, retorna 0
# [7,0,2] -> len() == 1 não, 7 < 0 não, retorna 0
# [0,2] -> len() == 1 não, 0 < 2 sim, retorna 0
# [2] -> len() == 1 sim, -> retorna 2

produtos = []
while(True):
    nome = input("Digite o nome de um produto: ")
    preco = float(input("Digite o preço de um produto: "))
    produto = Produto(nome, preco)
    produtos.append(produto)
    cont = input("Continuar? (S/N) ").upper()
    while (not re.search("^[SN]$", cont)):
        cont = input("\033[94mContinuar? (S/N) \033[0m").upper()
    if(cont == 'N'):
        break
informacoes(produtos)