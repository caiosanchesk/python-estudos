def tab(x):
    i = 1
    while(i < 11):
        print("{} x {} = {}".format(x, i, x * i))
        i+=1
while(True):
    numero = int(input("Digite um número (negativo para parar) para sua tabuada: "))
    if(numero < 0): break
    print("="*20)
    tab(numero)
    print("="*20)