from module.utilidades import menu
from module.utilidades import app

SEPARADOR = '*&@'

def obtemOpcao():
    while(True):
        try:
            return int(input("Digite a opção: "))
        except:
            print("ERRO, TENTE NOVAMENTE!")

def inicia():
    while(True):
        menu.imprimeMenu()
        opcao = obtemOpcao()
        match(opcao):
            case 1: app.opcao1(SEPARADOR)
            case 2: app.opcao2(SEPARADOR)
            case 3: app.opcao3(SEPARADOR)
            case 4: app.opcao4(SEPARADOR) 
            case 5: 
                app.opcao5()
                break
            case _: app.erro() 
inicia()