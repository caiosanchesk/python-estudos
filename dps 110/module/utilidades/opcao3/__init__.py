from module.utilidades import pessoa as ps
from module.utilidades import aux_ as ax

def imprimeOpcao(): print("="*20+"\n    OPÇÃO 3    \n"+"="*20)

def modificaPessoa(pessoas: list[ps.Pessoa]):
    indice = ax.buscaIndice()
    pessoa = ax.buscaPessoa()
    pessoas[indice] = pessoa
    return pessoas
