from module.utilidades import pessoa as ps
from module.utilidades import aux_ as ax

def imprimeOpcao(): print("="*20+"\n    OPÇÃO 2    \n"+"="*20)

def cadastraPessoa(pessoas: list[ps.Pessoa]):
    pessoa = ax.buscaPessoa()
    pessoas.append(pessoa)
    return pessoas
