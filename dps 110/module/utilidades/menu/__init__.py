def imprimeMenu():
    print("="*20+"\n    MENU PRINCIPAL    \n"+"="*20)
    print("1 - Ver Pessoas Cadastradas")
    print("2 - Cadastrar pessoa")
    print("3 - Modificar pessoa")
    print("4 - Deletar pessoa")
    print("5 - Sair do Sistema")