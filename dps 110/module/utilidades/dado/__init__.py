def leiaDinheiro(msg):
    entrada = str(input(msg)).replace(',', '.')
    while (entrada.isalpha()):
        print("ERRO!")
        entrada = str(input(msg)).replace(',', '.')
    return float(entrada)