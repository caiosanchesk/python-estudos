from module.utilidades import pessoa as ps

def formataLinhas(pessoas: list[ps.Pessoa], SEPARADOR: str):
    linhas_arquivo = []
    for p in pessoas:
        linha = SEPARADOR.join([p.nome, str(p.idade)]).replace("\n", '')
        linhas_arquivo.append(linha)
    return "\n".join(linhas_arquivo)

def formataPessoas(linhas: list[str], SEPARADOR):
    pessoas = []
    for p in linhas:
        p.replace('\n', '')
        pessoa = p.split(SEPARADOR)
        pessoa_objeto = ps.Pessoa(pessoa[0], pessoa[1])
        pessoas.append(pessoa_objeto)
    return pessoas

def buscaIndice(tipo='modificada'):
    return int(input("Digite o número da pessoa a ser {}: ".format(tipo)))

def buscaPessoa():
    nome = buscaNome()
    idade = buscaIdade()
    return ps.Pessoa(nome, idade)

def buscaIdade():
    return int(input("Digite a idade: "))

def buscaNome():
    return input("Digite o nome: ")