from module.utilidades import pessoa as ps

def imprimeOpcao(): print("="*20+"\n    OPÇÃO 1    \n"+"="*20)

def imprimeArquivo(pessoas: list[ps.Pessoa]):
    print("-"*50)
    print(f"{'id':^7}|{'   nome':<30}|{'idade':^11}")
    print("-"*50)
    for i in range(len(pessoas)):
        nome = str(pessoas[i].nome).replace("\n", '')
        idade = str(pessoas[i].idade).replace("\n", '')
        print(f"{i:^7}|{'   '+nome:<30}|{idade:^11}\n"+"-"*50)
