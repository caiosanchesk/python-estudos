from module.utilidades import opcao1 as op1
from module.utilidades import opcao2 as op2
from module.utilidades import opcao3 as op3
from module.utilidades import opcao4 as op4
from module.utilidades import opcao5 as op5
from module.utilidades import arquivo as arq
from module.utilidades import aux_ as ax

def arquivo(nome='pessoas.txt'):
    if(arq.arquivoExiste(nome)): 
        arquivo = arq.abreArquivo(nome)
        return arq.lerArquivo(arquivo)
    else: 
        print("Erro ao buscar o arquivo!\n Criando arquivo {}".format(nome))

def opcao1(SEPARADOR):
    op1.imprimeOpcao()
    linhas_arquivo = arquivo()
    pessoas = ax.formataPessoas(linhas_arquivo, SEPARADOR)
    op1.imprimeArquivo(pessoas)

def opcao2(SEPARADOR):
    op2.imprimeOpcao()
    linhas_arquivo = arquivo()
    pessoas = ax.formataPessoas(linhas_arquivo, SEPARADOR)
    pessoas_ = op2.cadastraPessoa(pessoas)
    linhas_arquivo = ax.formataLinhas(pessoas_, SEPARADOR)
    arq.escreveArquivo('pessoas.txt', linhas_arquivo)

def opcao3(SEPARADOR):
    op3.imprimeOpcao()
    linhas_arquivo = arquivo()
    pessoas = ax.formataPessoas(linhas_arquivo, SEPARADOR)
    pessoas_ = op3.modificaPessoa(pessoas)
    linhas_arquivo = ax.formataLinhas(pessoas_, SEPARADOR)
    arq.escreveArquivo('pessoas.txt', linhas_arquivo)

def opcao4(SEPARADOR):
    op4.imprimeOpcao()
    linhas_arquivo = arquivo()
    pessoas = ax.formataPessoas(linhas_arquivo, SEPARADOR)
    pessoas_ = op4.deletaPessoa(pessoas)
    linhas_arquivo = ax.formataLinhas(pessoas_, SEPARADOR)
    arq.escreveArquivo('pessoas.txt', linhas_arquivo)

def opcao5(): op5.imprimeOpcao()

def erro():
    print("TENTE DE NOVO")