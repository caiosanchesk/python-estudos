def arquivoExiste(nome):
    try:
        file = abreArquivo(nome)
        file.close()
        return True
    except FileNotFoundError:
        print('O arquivo não pôde ser aberto ou não existe!')


def abreArquivo(nome, tipo='rt'):
    return open(nome, tipo)

def criaArquivo(nome):
    try:
        nome = abreArquivo(nome, 'wt+')
        print("Arquivo criado com sucesso! ")
    except: print("ERRO AO CRIAR O ARQUIVO!")

def lerArquivo(arquivo):
    return arquivo.readlines()

def escreveArquivo(nome, linhas_arquivo):
    arquivo = abreArquivo(nome, 'w')
    arquivo.write(linhas_arquivo)

