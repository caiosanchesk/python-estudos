from module.utilidades import pessoa as ps
from module.utilidades import aux_ as ax

def imprimeOpcao(): print("="*20+"\n    OPÇÃO 4    \n"+"="*20)

def deletaPessoa(pessoas: list[ps.Pessoa]):
    indice = ax.buscaIndice('deletada')
    pessoas.pop(indice)
    return pessoas