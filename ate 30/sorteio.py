import random as rd

def sorteia(alunos):
    return rd.choice(alunos)


alunos = []
i = 0
while(i < 4):
    aluno = input("Aluno "+str(i+1)+": ")
    alunos.append(aluno)
    i+=1

print("Aluno escolhido: " + sorteia(alunos))

