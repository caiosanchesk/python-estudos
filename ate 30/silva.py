import re

def verificaSilva(nome):
    return re.search("^.*silva.*$", nome, re.IGNORECASE) != None

nome = input("Digite seu nome: ")
print(verificaSilva(nome))
print("{}".format('silva' in nome.lower()))