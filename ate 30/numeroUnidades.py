def imprimeDados(numero: str):
    dadosNumero = numero.zfill(4)
    print("Unidade: " + dadosNumero[3])
    print("Dezena: " + dadosNumero[2])
    print("Centena: " + dadosNumero[1])
    print("Milhar: " + dadosNumero[0])

numero = input("Digite um número de 0 a 9999: ")
imprimeDados(numero)