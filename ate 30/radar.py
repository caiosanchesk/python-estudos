import locale
locale.setlocale(locale.LC_ALL, 'pt-BR.UTF-8')
def calculaValor(km):
    return km * 7

kmAcima = input("Digite quantos km acima você andou: ")
reais = locale.format_string("%.2f", calculaValor(float(kmAcima)), grouping=True)
print("Boa, seu animal, agora tem que pagar R$ " + reais + " de multa!")