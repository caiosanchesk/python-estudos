def soma(x, y): return x + y
def mult(x, y): return x * y
def sub(x, y): return x - y
def div(x, y):
    if(y == 0):
        print('Não existe divisão por 0!')
        return
    return x / y

cont = 'S'
while(cont == 'S' or cont == 's'):
    x = int(input('Digite o número 1: '))
    y = int(input('Digite o número 2: '))
    print('Escolha uma opção: \n[1] Soma\n[2] Multiplicação\n[3] Subtração\n[4] Divisão:\n')
    opcao = int(input(':'))
    match opcao:
        case 1:
            print('Resultado: ' + str(soma(x,y)))
        case 2:
            print('Resultado: ' + str(mult(x,y)))
        case 3:
            print('Resultado: ' + str(sub(x,y)))
        case 4:
            print('Resultado: ' + str(div(x,y)))
        case _: 
            print('Opção inválida!')
    
    cont = input('Deseja continuar? (S ou s) para sim, qualquer outro valor para não: ')