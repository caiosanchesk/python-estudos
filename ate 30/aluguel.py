import locale

def calculaDias(dias):
    return dias * 60

def calculaKm(km):
    return km * 0.15

def calculaValor(dias, km):
    return calculaDias(dias) + calculaKm(km)

dias = int(input("DIAS: "))
km = int(input("KM: "))

print("VALOR: " + str(calculaValor(dias, km)))