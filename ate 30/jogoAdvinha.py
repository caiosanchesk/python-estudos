import random as rd

def sorteia():
    return rd.randint(0, 5)

def valida(numero):
    if(int(numero) == sorteia()):
        print("Parabéns, você venceu!")
    else:
        print("Que pena, você perdeu! O número era " + str(sorteia()))

numero = input("Digite um número entre 0 e 5: ")
valida(numero)