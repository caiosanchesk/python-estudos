import re

# tamanho de 8 a 16, duas letras maiúsculas, duas minúsculas, um número e um caractere especial
def validacao(txt): 
    return re.search("^(?=.*[A-Z].*[A-Z])(?=.*\d)(?=.*[^\w\s]).{8,16}$", txt)

while (True): 
    txt = input("Digite uma senha de amanho de 8 a 16 caracteres, com pelo menos duas letras maiúsculas, duas minúsculas, um número e um caractere especial: ")

    if(validacao(txt)):
        print("Esta é uma senha válida!\n")
    else:
        print('Esta é uma senha inválida!\n')
    
    continuar = input('Continuar? (S|s)im ou não? (qualquer valor): ')
    if(continuar != 'S' and continuar != 's'):
        break

print('Programa encerrado pelo usuário.') 