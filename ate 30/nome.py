def imprimeNome(nome: str):
    print("Seu nome é " + nome.lower())
    print("SEU NOME É " +nome.upper())

def contaNome(nome: str):
    nome = nome.replace(" ", "")
    print("Seu nome completo tem " + str(len(nome)) + " letras")

def nomeInicial(nome: str):
    nomeInicial = (nome.split(" "))[0]
    print("Seu primeiro nome é " + nomeInicial + " e ele tem " + str(len(nomeInicial)) + " letras")

nome = input("Digite o seu nome completo: ")

imprimeNome(nome)
contaNome(nome)
nomeInicial(nome)