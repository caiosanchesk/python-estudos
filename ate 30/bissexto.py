def verificaSeculo(ano):
    return ano % 400 == 0 

def verificaAno(ano):
    return ano % 4 == 0

def verificaBissexto(ano):
    if(ano % 100 == 0):
        return verificaSeculo(ano)
    return verificaAno(ano)   

ano = 1900
ver = verificaBissexto(ano)
print(ver)