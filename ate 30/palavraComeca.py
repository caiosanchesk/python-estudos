import re

def verificaInicio(cidade):
    return re.search("^Santo.*$", cidade, re.IGNORECASE) != None

cidade = input("Digite sua cidade: ")
if(verificaInicio(cidade)):
    print("Sua cidade começa com S(s)anto! ")
else:     
    print("Sua cidade NÃO começa com S(s)anto! ")
