import random as rd

def sorteia(alunos):
    return rd.choice(alunos)

def sorteiaOrdem(alunos: list):
    alunosSorteados = []
    while(len(alunos) >= 1):
        aluno = sorteia(alunos)
        alunosSorteados.append(aluno)
        alunos.remove(aluno)
    return alunosSorteados

alunos = []
i = 1
while(i <= 4):
    aluno = input("Aluno "+str(i)+": ")
    alunos.append(aluno)
    i+=1

alunosSorteados = sorteiaOrdem(alunos)
print("Ordem: ")
i = 1
for x in alunosSorteados:
    print("Aluno "+ str(i)+ ": " + str(x))
    i+=1