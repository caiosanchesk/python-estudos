import math

def calculaHipotenusa(adj, op):
    if(adj <= 0 | op <=0):
        print("Valor inválido!\n")    
        return 0
    return math.sqrt(math.pow(adj, 2) + math.pow(op, 2))

adj = int(input("Cateto adjacente: "))
op = int(input("Cateto oposto: "))

print("Hipotenusa: " + str(calculaHipotenusa(adj, op)))
