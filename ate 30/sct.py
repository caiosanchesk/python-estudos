import math

def imprimeValores(angulo):
    print("Seno: " + str(round(math.sin(angulo), 2)))
    print("Cosseno: " + str(round(math.cos(angulo), 2)))
    print("Tangente: " + str(round(math.tan(angulo), 2)))


angulo = math.radians(float(input("Ângulo: ")))

imprimeValores(angulo)