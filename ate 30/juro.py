def calculaJuro(capital, tempo, taxa):
    return round((capital * ((1 + taxa)**tempo)), 2)

def criaParcelas(resultado, tempo):
    parcelas = []
    for x in range(tempo):
        parcelas.append({"numero": x, "valor": round((resultado / tempo), 2)})
    return parcelas

capital = float(input('Digite o capital inicial: '))
tempo = int(input('Digite o tempo: '))
taxa = float(input('Digite a taxa: ')) / 100
resultado = str(calculaJuro(capital, tempo, taxa))
print('Resultado total: ' + resultado)
print('\n\nParcelas: \n')

parcelas = criaParcelas(float(resultado), tempo)
for x in range(tempo):
    print(parcelas[x])