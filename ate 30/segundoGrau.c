#include <stdio.h>
#include <math.h>

double delta(double a, double b, double c){
    return pow(b,2) - 4 * a * c;
}

void resultado(double *resultado, double delta, double a, double b){
    resultado[0] = ((-b) + sqrt(delta)) / 2 * a;
    resultado[1] = ((-b) - sqrt(delta)) / 2 * a;
}

int main(){
    double resultado[2];
    double deltaR = delta(1,2,1);
    printf("%f", deltaR);
    return 0;
}


