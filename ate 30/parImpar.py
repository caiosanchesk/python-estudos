def parImpar(numero):
    return 'PAR' if (numero % 2) == 0 else 'IMPAR'

numero = input("Digite um número para ver se é par ou ímpar: ")
print(parImpar(int(numero)))