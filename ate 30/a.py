import re

def encontraQtd(palavra):
    return len(re.findall('[a]{1}', palavra, re.IGNORECASE))

def encontraPosPrimeira(palavra):
    return re.search('[a]{1}', palavra, re.IGNORECASE).pos + 1

def encontraPosUltima(palavra, ocorrencias):
    objUltIterador = re.finditer('[a]{1}', palavra, re.IGNORECASE)
    i = 1
    for x in objUltIterador:
        if(i == ocorrencias):
            return x.start() + 1
        i+=1
        
def encontraA(palavra):
    ocorrencias = encontraQtd(palavra)
    print("A letra A aparece " + str(ocorrencias) + " vezes na tela")
    print("A letra A aparece primeiramente na posição: " + str(encontraPosPrimeira(palavra)))
    print("A letra A aparece por último na posição: " + str(encontraPosUltima(palavra, ocorrencias)))


encontraA('Amanda ama Pedro')