def encontraPrimeiro(nome: str):
    return (nome.split(" "))[0]

def encontraUltimo(nome: str):
    qtdNomes = nome.split(" ") 
    return qtdNomes[len(qtdNomes) - 1]

nome = input("Digite seu nome completo: ")
print("Seu primeiro nome é: " + encontraPrimeiro(nome))
print("Seu último nome é: " + encontraUltimo(nome))