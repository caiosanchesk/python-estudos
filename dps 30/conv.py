def binario(numero):
    return bin(numero)

def hexa(numero):
    return hex(numero)

def octal(numero):
    return oct(numero)

numero = int(input("Digite um número inteiro: "))
opcao = int(input("Digite uma opção de conversão: \n[1] Binário\n[2] Hexadecimal\n[3] Octal\n: "))
match(opcao):
    case 1:
        print("Resultado em binário: " + str(binario(numero)))
    case 2:
        print("Resultado em hexadecimal: " + str(hexa(numero)))
    case 3:
        print("Resultado em octal: " + str(octal(numero)))
    case _:
        print("Opção inválida!")
        print("Resultado em binário: " + str(binario(numero)))
        print("Resultado em hexadecimal: " + str(hexa(numero)))
        print("Resultado em octal: " + str(octal(numero)))