def verificaFinanciamento(financiamento, salario, anos):
    qtd_parcelas = int(anos * 12)
    parte_salario = round(salario * 0.3, 2)
    valor_parcela = round(financiamento / qtd_parcelas, 2)
    if(parte_salario > valor_parcela):
        print("Seu salário: " + str(salario))
        print("Parabéns! O seu financiamento de " + str(financiamento) + " foi aprovado! As " + str(qtd_parcelas) + " parcelas terão o valor de " + str(valor_parcela) + " cada uma")
        return
    print("O seu financiamento de " + str(financiamento) + " foi NEGADO! As " + str(qtd_parcelas) + " parcelas teriam o valor de " + str(valor_parcela) + " cada uma. Mas o seu valor é maior que 30% "+"do seu salário! (" + str(parte_salario) +")")

financiamento = float(input("Digite o valor do financiamento: "))
anos = float(input("Digite a quantidade de anos: "))
salario = float(input("Digite o seu salário: "))

verificaFinanciamento(financiamento, salario, anos)
