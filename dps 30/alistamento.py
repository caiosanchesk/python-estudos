import datetime as dt

def calculaIdade(nascimento, anoAtual):
    return anoAtual - nascimento

def calculaAlistamento(nascimento):
    return nascimento + 18

def verificaAlistamento(nascimento):
    anoAtual = dt.datetime.now().year
    idade = calculaIdade(nascimento, anoAtual)
    dados = "Você nasceu em " + str(nascimento) + " e tem " + str(idade) + " em " + str(anoAtual)
    anoAlistamento = calculaAlistamento(nascimento)
    if(idade > 18):
        return dados + ".\nE preisava se alistar em " + str(anoAlistamento) + ".\nSeu alistamento foi há " + str(anoAtual - anoAlistamento) + " ano(s)!" 
    if(idade < 18):
        return dados + ".\nE precisa de alistar em " + str(anoAlistamento) + ".\nSeu alistamento será em " + str(anoAlistamento - anoAtual) + " ano(s)!" 
    return dados + ".\nVocê deve se alistar IMEDIATAMENTE!"

nascimento = int(input("Digite seu ano de nascimento: "))
print(verificaAlistamento(nascimento))