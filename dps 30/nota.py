def verificaMedia(media):
    if(media < 5):
        return "REPROVADO!"
    if(media < 7):
        return "RECUPERAÇÃO!"
    return "APROVADO!"


def calculaMedia(nota1, nota2):
    return round((nota1 + nota2) / 2, 2)

nota1 = float(input("Digite sua nota 1: "))
nota2 = float(input("Digite sua nota 2: "))

media = calculaMedia(nota1, nota2)
print("Sua média é: " + str(media) + " assim, você está: " + verificaMedia(media))