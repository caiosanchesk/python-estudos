def calculaAumento(salario):
    return salario + (salario * 0.10) if salario > 1250 else salario + (salario * 0.15)

salario = float(input("Digite o valor do salário: "))
print("Valor recalculado: " + str(calculaAumento(salario)))