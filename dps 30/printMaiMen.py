def verifica(num1, num2):
    if(num1 > num2):
        return "O primeiro valor é maior"
    if(num1 < num2):    
        return "O segundo valor é maior"
    return "Nenhum é maior, os dois são iguais!"

num1 = int(input("Digite o primeiro valor: "))
num2 = int(input("Digite o segundo valor: "))

print(verifica(num1, num2))