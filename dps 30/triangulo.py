def somaPartes(b, c):
    return b + c

def verificaTriangulo(a, b, c):
    v_a = somaPartes(b,c) > a
    v_b = somaPartes(a,c) > b
    v_c = somaPartes(a,b) > c

    if(v_a and v_b and v_c):
        print("As partes PODEM formar um triângulo!")
        return
    print("As partes NÃO PODEM formar um triângulo!")

a = float(input("Lado A: "))
b = float(input("Lado B: "))
c = float(input("Lado C: "))

verificaTriangulo(a, b, c)