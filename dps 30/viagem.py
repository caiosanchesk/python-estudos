def calculaGasto(km: int):
    return round((km * 0.5 if km <= 200 else km * 0.45), 2)

km = int(input("Digite quantos km é a viagem: "))
print("Seu gasto será de " + str(calculaGasto(km)))