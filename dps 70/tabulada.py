from produto import Produto

def imprimeProdutos(produtos):
    for i in range(len(produtos)):
        print(f"{produtos[i].nome:.<30}"+f"R$ {produtos[i].preco:>7.2f}")

produtos: tuple = ()
qtd = int(input("Digite a quantidade de produtos para serem cadastrados: "))
for i in range(0, qtd):
    nome = input("\nDigite o nome do {}º produto: ".format(i + 1))
    preco = float(input("Digite o preço do {}º produto: ".format(i + 1)))
    produto = Produto(nome, preco)
    produtos += (produto,)

print("="*20)
print("\nLISTAGEM DE PRODUTOS\n")
imprimeProdutos(produtos)