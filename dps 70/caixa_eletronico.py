def caixa(valor):
    qtd_cinq = cinq(valor)
    valor = valor % 50
    qtd_vinte = vinte(valor)
    valor = valor % 20
    qtd_dez = dez(valor)
    valor = valor % 10
    qtd_um = valor
    
    print("Serão entregues: ")
    inf(qtd_cinq, 50)
    inf(qtd_vinte, 20)
    inf(qtd_dez, 10)
    inf(qtd_um, 1)

def inf(quantidade, valor):
    if(quantidade > 0):
        print("{} notas de R$ {}".format(quantidade, valor))

def cinq(valor): return int(valor / 50)
def vinte(valor): return int(valor / 20)
def dez(valor): return int(valor / 10)

valor = int(input("Digite um valor inteiro para ser sacado: "))
caixa(valor)