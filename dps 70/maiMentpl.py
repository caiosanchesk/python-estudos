import random as rd
def maior(tuple):
    return sorted(tuple, reverse=True)[0]
def menor(tuple):
    return sorted(tuple)[0]

def gerarAleatorio(qtd):
    numeros: tuple = ()
    for i in range(0, qtd):
        numeros += (rd.randint(0, (qtd * 10)), )
    return numeros

qtd = int(input("Digite a quantidade de números para serem gerados: "))
numeros = gerarAleatorio(qtd)
mai = maior(numeros)
men = menor(numeros)
print("Os números são: {}\nO maior número é: {}\nO menor número é: {}".format(numeros, mai, men))
