def analisarPalavras(palavras):
    for i in palavras:
        print("\nNa palavra {} temos as vogais: ".format(i), end="")
        for l in i:
            if(l.lower() in 'aeiou'):
                print(l,end=" ")

palavras = ()
qtd = int(input("Digite a quantidade de palavras para serem analisadas: "))
for i in range (0, qtd):
    palavra = input("Digite a {}ª palavra: ".format(i + 1)).upper()
    palavras += (palavra,)
analisarPalavras(palavras)