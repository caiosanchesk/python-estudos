valores = ('zero', 'um', 'dois', 'três', 'quatro', 'cinco',
 'seis', 'sete', 'oito', 'nove', 'dez', 'onze',
 'doze', 'treze', 'catorze', 'quinze', 'dezesseis',
 'dezessete', 'dezoito', 'dezenove', 'vinte')

while(True):
    numero = int(input("Digite um número entre 0 e 20 (inteiro): "))
    if(1 <= numero <= 20):
        break
    print("Tente novamente!")
print("Você digitou o número {} ({})".format(numero, valores[numero]))