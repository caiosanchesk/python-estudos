def insereLista(numeros: list, numero):
    numeros.append(numero)
    if(len(numeros) == 1):
        return numeros
    i = len(numeros) - 2
    while(numeros[i] > numero):
        numeros[i + 1] = numeros[i]
        numeros[i] = numero
        i -= 1
        if(i == -1): break
    return numeros

numeros = []

qtd = int(input("Qauantos números quer adicionar na lista? "))
for i in range(qtd):
    numero = int(input("Digite o {}º número: ".format(i + 1)))
    numeros = insereLista(numeros, numero)
    print("="*20+"RESULTADO APÓS A {}ª INSERÇÃO".format(i + 1)+"="*20+"\n{}".format(numeros))


print("="*20+"RESULTADO FINAL"+"="*20+"\n{}".format(numeros))