

numeros = []
qtd = int(input("Digite a quantidade de números para adicionar na lista: "))
for i in range(qtd):
    numero = int(input("Digite o {}º número: ".format(i + 1)))
    numeros.append(numero) if numero not in numeros else print("Número existente!")
numeros.sort()
print("Esses são os números adicionados: {}".format(numeros))