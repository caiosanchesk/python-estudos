def achar(numeros: tuple, numero = 3):
    return numeros.index(numero) if numero in numeros else - 1

def nove(numeros: tuple):
    qtd_nove = 0
    for i in range(len(numeros)):
        if(numeros[i] == 9): 
            qtd_nove += 1
    return qtd_nove

def par(numeros: tuple):
    qtd_par = 0
    for i in range(len(numeros)):
        if(numeros[i] % 2 == 0): 
            qtd_par += 1 
    return qtd_par

numeros = ()
for i in range(1, 5):
    numero = int(input("Digite o número {}: ".format(i)))
    numeros += (numero,)

print("Os números são: {}\n".format(numeros))

index_tres = achar(numeros) + 1 if achar(numeros) >= 0 else "nenhuma"
qtd_nove = nove(numeros)
qtd_par = par(numeros)
print("O valor 9 apareceu {} vezes\nO valor 3 apareceu primeiramente na posição {}\nHá {} números pares nessa sequência".format(qtd_nove, index_tres, qtd_par))