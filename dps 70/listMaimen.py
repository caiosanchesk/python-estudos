import random as rd

def maior(numeros: list):
    numeros.sort(reverse=True)
    return numeros[0]

def menor(numeros: list):
    numeros.sort()
    return numeros[0]

numeros = []
qtd = int(input("Digite a quantidade de números: "))
for i in range(qtd):
    numero = rd.randint(0, qtd * 10)
    numeros.append(numero)

menor_numero = menor(numeros)
index_menor = numeros.index(menor_numero) + 1
maior_numero = maior(numeros)
index_maior = numeros.index(maior_numero) + 1

print("O maior número é: {}, ele está na {}ª posição\nO menor número é: {}, ele está na {}ª posição".format(maior_numero, index_maior, menor_numero, index_menor))