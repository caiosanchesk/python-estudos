def maiorMenor(qtd):
    maior = 0
    menor = 0
    for i in range(1, qtd + 1):
        i = int(input("Digite o peso da {}ª pessoa: ".format(i)))
        if(i > maior): maior = i
        if(i < menor or menor == 0): menor = i
    print("O maior peso é: {} quilos".format(maior))
    print("O menor peso é: {} quilos".format(menor))

qtd_pessoas = int(input("Digite a quantidade de pessoas para analisar seu peso: "))
maiorMenor(qtd_pessoas)