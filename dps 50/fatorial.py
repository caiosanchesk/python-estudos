def fatorial(x):
    resultado = 1
    for i in range(2, x + 1):
        resultado *= i
    return resultado

numero = int(input("Digite um número para calcular seu fatorial: "))
print("O fatorial de {} é: {}".format(numero, fatorial(numero)))