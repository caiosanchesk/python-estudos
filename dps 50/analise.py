def analise(pessoas: list):
    soma = 0
    maiorIdade = 0
    velho = 'Nenhum'
    qtd_mulheres = 0    
    for i in range(0, len(pessoas)):
        pessoa = pessoas[i] 
        idade = pessoa["idade"]
        soma += idade
        if( pessoa["sexo"] == 'M' ):
            if(idade > maiorIdade):
                velho = pessoa["nome"]
        elif(idade < 20):
            qtd_mulheres += 1
    media = soma / len(pessoas)
    print("A média de idade do grupo é: {}".format(media))
    print("O homem mais velho é: {}".format(velho))
    print("Existem {} mulheres com menos de 20 anos".format(qtd_mulheres))

pessoas = []
qtd = int(input("Digite a quantidade de pessoas para analisar: "))
for i in range(1, qtd + 1):
    nome = input("Digite o nome da {}ª pessoa: ".format(i))
    idade = int(input("Digite a idade da {}ª pessoa: ".format(i)))
    sexo = input("Digite o sexo (M ou F) da {}ª pessoa: ".format(i)).upper()
    pessoa = {"nome": nome, "idade": idade, "sexo": sexo}
    pessoas.append(pessoa)
analise(pessoas)
