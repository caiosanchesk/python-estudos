import random as rd
frases_erro_jogador = [
    "\033[34mMeu, assim não dá, bora se ligar e focar mais!\033[0m",  # Azul
    "\033[36mBrother, isso tá na cara que não tá dando certo. Dá uma acordada!\033[0m",  # Ciano
    "\033[96mPô, que vacilo! Bora tentar de novo com mais cuidado, beleza?\033[0m",  # Ciano claro
    "\033[94mCara, esses erros tão atrapalhando tudo. Vamo dar um gás pra superar!\033[0m",  # Azul claro
    "\033[92mTô quase perdendo a paciência, mano. Melhora isso logo!\033[0m",  # Verde claro
    "\033[38;5;226mSe não tá rolando, é hora de mudar de estratégia, sacou?\033[0m",  # Amarelo claro
    "\033[93mNão tô acreditando nessa bola fora, meu! Foca aí e acerta de uma vez!\033[0m",  # Amarelo
    "\033[38;5;208mCara, já vi moleque fazendo melhor que isso. Dá um gás aí!\033[0m",  # Laranja
    "\033[91mTô começando a desconfiar das tuas habilidades, irmão. Bora provar que tô errado.\033[0m",  # Vermelho
    "\033[38;5;196mSe não tá dando conta, talvez seja hora de cair fora, né? Não tenho tempo pra amadorismo.\033[0m"  # Vermelho carmesim
]

computador = rd.randint(0, 10)
jogador = int(input("Digite um número entre 0 e 10: "))
i = 0
while(True):
    if(jogador == computador):
        print("\033[92mParabéns, você acertou! \033[0m")
        break
    jogador = int(input(frases_erro_jogador[i] + ": "))
    i+=1
    if i == 10 : break