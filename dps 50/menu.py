def soma(x, y): return x + y
def mult(x, y): return x * y
def maior(x, y): 
    if(x > y): return x
    return y

def selecionarNumeros():
    x = int(input("Digite um número: "))
    y = int(input("Digite outro número: "))
    return [x, y]

[x, y] = selecionarNumeros()

while(True):
    opcao = int(input("\033[205mEscolha uma opção:\n\033[0m"
                    "\033[94m[1] \033[0m\033[34mSomar\033[0m"
                    "\033[94m\n[2] \033[0m\033[36mMultiplicar\033[0m"
                    "\033[94m\n[3] \033[0m\033[92mMostrar Maior\033[0m"
                    "\033[94m\n[4] \033[0m\033[93mNovos números\033[0m"
                    "\033[94m\n[5] \033[0m\033[38;5;196mSair do programa\033[0m\n: "))

    match (opcao):
        case 1: print("{} + {} = {}".format(x, y, soma(x, y)))
        case 2: print("{} x {} = {}".format(x, y, mult(x, y)))
        case 3: print("O maior entre {} e {} é: {}".format(x, y, maior(x, y)))
        case 4: [x, y] = selecionarNumeros()
        case 5: break
        case _: print("Opção inválida!")

