from math import sqrt

def primo(numero):
    for i in range(2, int(sqrt(numero)) + 1):
        if(numero % i == 0):
            return False
    return True

numero = int(input("Digite um número qualquer: "))
if(primo(numero)):
    print("É primo!")
else: 
    print("Não é primo!")