import re

def valida(sexo):
    return re.search('^[MmFf]{1}$', sexo)

sexo = input("Digite o seu sexo M ou F: ")
while(True):
    if(valida(sexo)):
        print("Sexo {} registrado com sucesso!".format(sexo.upper()))
        break
    sexo = input("\033[91mo seu merda, digite corretamente o seu sexo M ou F: \033[0m")