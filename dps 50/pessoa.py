import datetime as dt

def analisaPessoas(x, maioridade):
    atual = dt.datetime.now().year
    totMaior = 0
    totMenor = 0
    for p in range(1, x + 1):
        ano = int(input("Em que ano a {}ª pessoa nasceu? ".format(p)))
        idade = atual - ano
        if(idade >= maioridade):
            totMaior += 1
        else: totMenor += 1
    print("Ao todo tivemos {} pessoas maiores de idade".format(totMaior))
    print("Ao todo tivemos {} pessoas menores de idade".format(totMenor))


maioridade = int(input("Digite a idade da maioridade: "))
qtd_pessoas = int(input("Digite a quantidade de pessoas que você quer analisar: "))

analisaPessoas(qtd_pessoas, maioridade)