def palindromo(palavra: str):
    palavraFormatada = list(palavra.replace(" ", ""))
    palavraInversa = list(palavra.replace(" ", ""))
    palavraInversa.reverse()

    if(str(palavraFormatada) == str(palavraInversa)):
        return "É UM PALÍNDROMO!"
    return "NÃO É UM PALÍNDROMO!"

palavra = input("Digite uma palavra/frase para verificação de palíndromo: ")
print(palindromo(palavra))