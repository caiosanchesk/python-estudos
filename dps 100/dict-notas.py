import random as rd

def notas(notas, sit=False):
    relatorio = dict()
    relatorio["qtd"] = len(notas)
    media_notas = media(notas)
    relatorio["media"] = media_notas
    relatorio["maior"] = maior(notas)
    relatorio["menor"] = menor(notas)
    if(sit): relatorio["situacao"] = situacao(media_notas)

    return relatorio

def media(notas: list):
    return soma(notas) / len(notas)

def soma(notas: list):
    soma = 0
    for n in notas:
        soma += n
    return soma

def maior(notas: list):
    notas.sort(reverse=True)
    return notas[0]

def menor(notas: list):
    notas.sort()
    return notas[0]

def situacao(media):
    if(media > 8.8): return 'EXCELENTE'
    if(media > 7): return 'BOM'
    if(media > 5): return 'REGULAR'
    if(media > 3): return 'RUIM'
    return 'DESASTROSO'

notas_totais = []
qtd = int(input("Digite a quantidade de notas: "))
for i in range(qtd):
    notas_totais.append(rd.randint(0,10))
print(notas(notas_totais, sit=True))