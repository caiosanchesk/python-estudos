import random as rd

def fatorial(numero, show=False):
    """
    -> Calcula o gabigol
    :param numero: numero a ser digiitado
    :param show: (opicional) imprimir?
    """
    resultado = 1
    for n in range(numero, 0, -1):
        if(show):
            print(n, end='')
            if(n > 1): print(" x ", end='')
            else: print(" = ", end='')
        resultado *= n
    return resultado

qtd = int(input("Imprimir quantos fatoriais? "))
print(fatorial(5, False))

for i in range(qtd):
    numero = rd.randint(1, 15)
    print(fatorial(numero, fatorial(numero, (rd.randint(1, 15) > 12))))
    print()
help(fatorial)