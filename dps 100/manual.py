c = [
    "\033[m",             # sem 0
    "\033[0;30;41m",      # vermelho 1
    "\033[0;30;42m",      # verde 2
    "\033[0;30;44m",      # azul 3 
    "\033[107m"          # branco 4
]

print(c)

def manual(func):
    titulo("ANALISANDO A FUNÇÃO \'{func}'\'", 3)
    print(c[4], end='')
    help(func)
    print(c[0], end='')

def titulo(msg, cor=0):
    tam = len(msg) + 4
    print(c[cor], end='')
    print("="*tam)
    print(f"  {msg}")
    print("="*tam)
    print(c[0])

while(True): 
    titulo("SISTEMA DE AJUDA GAB", 2)
    func = input("Digite a função que você quer ver: ")
    if(func.upper() == 'FIM'): 
        titulo("ATÉ LOGO!", 1)
        break
    manual(func)


