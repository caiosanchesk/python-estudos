import moeda as m
import re

valor = float(input("Digite um valor: "))
formata = input("FORMATAR? (S/N) ").upper()
while( not re.search('^[S|N]{1}$', formata) ):
    formata = input("digita certo: ").upper()

f = True if formata == 'S' else False

aumenta = m.aumenta(valor, 10, f)
diminui = m.diminui(valor, 10, f)
dobro = m.dobro(valor, f)
metade = m.metade(valor, f)

# valor = m.formatar(valor) if f else valor
# print("O valor {} aumentado em 10% é: {}".format(valor, aumenta))
# print("O valor {} diminuido em 10% é: {}".format(valor, diminui))
# print("O dobro do valor {} é: {}".format(valor, dobro))
# print("A metade do valor {} é: {}".format(valor, metade))

aumento = float(input("Digite o aumento: "))
reducao = float(input("Digite a redução: "))

m.resumo(valor, aumento, reducao)