def leia(msg):
    while(True):
        n = input(msg)
        if (n.isnumeric()): break
        print("\033[0;31mDIGITA UM NÚMERO VÁLIDO O ANIMAL!\033[m")
    return int(n)

n = leia("Digite um número válido: ")
print("Você digitou o número {}".format(n))