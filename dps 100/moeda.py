def formatar(valor, moeda = 'R$'):
    return f"{moeda} {valor:.2f}".replace('.',',')

def aumenta(valor, taxa, formata=True):
    valor = valor + (valor * taxa / 100)
    return formatar(valor) if formata else valor
def diminui(valor, taxa, formata=True):
    valor = valor - (valor * taxa / 100)
    return formatar(valor) if formata else valor

def dobro(valor, formata=True):
    valor = valor * 2
    return formatar(valor) if formata else valor

def metade(valor, formata=True):
    valor = valor / 2
    return formatar(valor) if formata else valor

def resumo(valor, aumento, reducao):
    print("~"*20)
    print("     RESUMO DO VALOR     ")
    print("~"*20)
    print("Preço analisado: {}".format(valor))
    print("Dobro do preço: {}".format(dobro(valor)))
    print("Metade do preço: {}".format(metade(valor)))
    print(str(aumento)+"% de aumento: {}".format(aumenta(valor, aumento)))
    print(str(reducao)+"% de redução: {}".format(diminui(valor, reducao)))