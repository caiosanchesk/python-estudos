def ficha(nome="<desconhecido>", gols=0):
    print("O jogador {} fez {} gol(s) no campeonato.\nPENA QUE NÃO É O GABIOL (SERIAM 45)".format(nome, gols))


nome = input("Digite o nome do jogador: ")
nome = "<desconhecido>" if nome.strip() == "" else nome
gols = input("Quantos gols o {} fez? ".format(nome))


gols = int(gols) if gols.isnumeric() and int(gols) >= 0 else 0

ficha(nome, gols)