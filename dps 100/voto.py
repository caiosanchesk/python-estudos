import datetime as dt
import random as rd


def voto(ano):
    anoAtual = dt.datetime.now().year
    if(anoAtual - ano < 16): return '\033[91mNEGADO'
    if(anoAtual - ano < 18): return '\033[92mOPCIONAL'
    return '\033[96mOBRIGATÓRIO' 

qtd = int(input("Diigte a quantidade de anos para analisar: "))
anoNascInicio = int(input("Digite o ano inicial do sorteio: "))
anoNascFim = int(input("Digite o ano final do sorteio: "))

for i in range(qtd):
    ano = rd.randint(anoNascInicio, anoNascFim)
    print("Se você nasceu em {}, a situação do seu voto é: {}\033[0m".format(ano, voto(ano)))