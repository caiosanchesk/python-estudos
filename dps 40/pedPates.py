import random as rd
import time

def jogo(jogador, computador):
    if(jogador == computador):
        return 'NENHUM'
    match (jogador):
        case 'Pedra':
            if(computador == 'Papel'):
                return 'JOGADOR'
            return 'COMPUTADOR'
        case 'Papel':
            if(computador == 'Tesoura'):
                return 'COMPUTADOR'
            return 'JOGADOR'
        case 'Tesoura':
            if(computador == 'Pedra'):
                return 'COMPUTADOR'
            return 'JOGADOR'
        

def apresenta(vencedor):
    print("O")
    time.sleep(2)
    print("OTÁRIO")
    time.sleep(2)
    print("É")
    time.sleep(2)
    print("VOCÊ")
    time.sleep(2)

    return "O VENCEDOR É: " + vencedor

opcoes = ['Pedra', 'Papel', 'Tesoura']
opcao = int(input("Escolha um: \n[0] Pedra\n[1] Papel \n[2] Tesoura\n: "))
if(opcao < 0 or opcao > 2):
    print("OPÇÃO INVÁLIDA")
else:     
    jogador = opcoes[opcao]
    computador = rd.choice(opcoes)
    vencedor = jogo(jogador, computador)
    dado = apresenta(vencedor)
    print("JOGADOR JOGOU: "+ jogador) 
    print("COMPUTADOR JOGOU: "+ computador)
    print(dado)