def imc(peso, altura):
    imc = round(peso / (pow(altura, 2)), 2)

    dado = "IMC: " + str(imc)
    if(imc < 18.5):
        return dado + " ABAIXO DO PESO"
    if(imc < 25):
        return dado + " PESO IDEAL"
    if(imc < 30):
        return dado + " SOBREPESO"
    if(imc < 40):
        return dado + " OBESIDADE"
    return dado + " OBESIDADE MÓRBIDA"

peso = float(input("Digite seu peso em quilos: "))
altura = float(input("Digite sua altura em metros: "))

print("SEU ESTADO É: \n" + imc(peso, altura))