def verificaTriangulo(a, b, c):
    return a + b > c and a + c > b and b + c > a

def categoriaTriangulo(a, b, c):
    if(not verificaTriangulo(a, b, c)):
        return "Não é possível formar um triângulo!"
    if(a == b or b == c or a == c):
        if(a != c or a != b):
            return "ISÓSCELES"
        return "EQUILÁTERO"
    return "ESCALENO"

a = int(input("Digite o valor de A no triângulo: "))
b = int(input("Digite o valor de B no triângulo: "))
c = int(input("Digite o valor de C no triângulo: "))

print(categoriaTriangulo(a, b, c))