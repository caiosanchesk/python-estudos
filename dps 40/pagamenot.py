def selecionarPagamento(preco):
    opcao = int(input("Selecione a forma de pagamento: \n[1] Á vista dinheiro/cheque\n[2] Á vista cartão\n[3] 2x no cartão\n[4] 3x ou mais no cartão\n: "))
    
    match(opcao):
        case 1:
            return "Sua compra de " + str(round(preco, 2)) + " vai custar " + str(round(preco - (preco * 0.1), 2))
        case 2:
            return "Sua compra de " + str(round(preco, 2)) + " vai custar " + str(round(preco - (preco * 0.05), 2))
        case 3:
            return "Sua compra de " + str(round(preco, 2)) + " vai custar o mesmo preço, e cada uma das 2 parcelas custará " + str(round(preco / 2, 2))
        case 4:
            precoAt = preco + (preco * 0.2)
            qtd_parcelas = int(input("Digite a quantidade de parcelas: "))
            return "Sua compra de " + str(round(preco, 2)) + " vai custar "+ str(round(precoAt, 2))+", e cada uma das "+str(qtd_parcelas)+" parcelas custará " + str(round(precoAt / qtd_parcelas, 2))
        case _: 
            return "Opção inválida!"    

preco = float(input("Digite o valor da compra: "))
print(selecionarPagamento(preco))