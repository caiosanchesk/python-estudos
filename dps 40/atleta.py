import datetime as dt

def calculaIdade(ano):
    return dt.datetime.now().year - ano

def verificaCategoria(idade):
    if(idade < 10):
        return "MIRIM"
    if(idade < 15):
        return "INFANTIL"
    if(idade < 20):
        return "JÚNIOR"
    if(idade < 26):
        return "SÊNIOR"
    return "MASTER"

ano = int(input("Digite o seu ano de nascimento: "))
idade = calculaIdade(ano)
print("Você tem " + str(idade) + " anos e está na categoria: " + verificaCategoria(idade))