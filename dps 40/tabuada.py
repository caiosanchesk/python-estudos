def tabuada(x):
    print("\nTabuada do " + str(x))
    for i in range (1, 10):
        print(str(x) + " x " + str(i) + " = " + str(x * i))


x = int(input("Digite um número de 1 a 9 para ver sua tabuada: "))
if(not isinstance(x, int)):
    for i in range (1, 10):
        tabuada(i)
else: 
    tabuada(int(x))