import re

def imprimeLista(pessoas):
    print("Foram cadastradas {} pessoas! ".format(len(pessoas)))
    print("A média de idade é: {}".format(media(pessoas)))
    mulheres_ = mulheres(pessoas)
    print("As mulheres são: ")
    imprimeMulheres(mulheres_)
    acima_media = acima(pessoas)
    imprimeAcima(acima_media)

def imprimeMulheres(mulheres):
    for i in mulheres:
        print(i["nome"])
        
def mulheres(pessoas):
    mulheres = []
    print(pessoas)
    for i in pessoas:
        if(i["sexo"] == 'F'): mulheres.append(i)
    return mulheres

def imprimeAcima(acima: list[dict]):
    print("As pessoas com peso acima da média são: ")
    for p in acima:
        for k,v in p.items():
            print(f"{k} = {v}", end=" ")
        print()
def acima(pessoas):
    media_acima = media(pessoas)
    acima = []
    print(pessoas)
    for i in pessoas:
        if(i["idade"] > media_acima): acima.append(i)
    return acima

def media(pessoas):
    soma = 0
    for i in pessoas:
        soma += i["idade"]
    return round(soma / len(pessoas), 2)

pessoas:list[dict] = []

while(True):
    pessoa = dict()
    pessoa["nome"] = input("Digite o nome: ")
    sexo = input("Digite o sexo da pessoa: [M/F] ").upper()
    while(not re.search("^[MF]{1}$", sexo)):
        sexo = input("DIGITA DIREITO ANIMAL! ")
    pessoa["sexo"] = sexo
    pessoa["idade"] = int(input("Digite a idade da pessoa: "))
    pessoas.append(pessoa)
    cont = input("Continuar? (S/N) ").upper()
    while(not re.search("^[SN]{1}$", cont)):
        cont = input("DIGITA DIREITO ANIMAL! ")
    if(cont == 'N'):
        break

imprimeLista(pessoas)