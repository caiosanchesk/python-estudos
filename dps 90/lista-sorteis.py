import random as rd

def sorteia(qtd):
    numeros = []
    for i in range(qtd):
        numeros.append(rd.randint(0, qtd * 10))
    return numeros

def somaPares(numeros):
    soma = 0
    for n in numeros:
        if(n % 2 == 0): soma += n
    return soma

qtd = int(input("Digite a quantidade de números: "))
numeros = sorteia(qtd)
print("Os números são: \n{}".format(numeros))
print("A soma dos números pares é: \n{}".format(somaPares(numeros)))