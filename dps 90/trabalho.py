import datetime as dt
def ctpsNaoZero(trabalho):
    trabalho['contratacao'] = int(input("Digite o ano de contratação: "))
    trabalho['salario'] = float(input("Digite o salário: "))
    trabalho['aposentadoria'] = 35 - (dt.datetime.now().year - trabalho['contratacao']) + trabalho['idade']
    return trabalho

trabalho = dict()

trabalho['nome'] = input("Digite o nome: ")
trabalho['idade'] = dt.datetime.now().year - int(input("Digite o ano de nascimento: "))
trabalho['ctps'] = int(input("Carteira de trabalho: (0 não tem) "))
trabalho = ctpsNaoZero(trabalho) if trabalho['ctps'] != 0 else trabalho

for k,v in trabalho.items():
    print(f"{k} tem o valor {v}")