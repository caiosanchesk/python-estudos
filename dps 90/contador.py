import time
def conta(inicio, fim, passo):
    while ( inicio != fim + passo):
        print(f"{inicio} ", end= "")
        time.sleep(0.8)
        inicio += passo
        if((passo > 0 and inicio > fim) or (passo < 0 and inicio < fim)): break
    print("FIM")

conta(1, 10, 1)
conta(10, 0, -2)
time.sleep(2)

inicio = int(input("\n\nInicio: "))
fim = int(input("Fim: "))
passo = int(input("Passo: "))

conta(inicio, fim, passo)