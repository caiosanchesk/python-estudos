import random as rd

def gerarAleatorio():
    return rd.randint(1,6)

jogadores = ['Carlos', 'Luiz', 'Jão', 'Ramires']

jogos = dict()
for i in jogadores: 
    jogos[i] = gerarAleatorio()

print("Os jogos foram: {}".format(jogos))
maior = None
for k,v in jogos.items():
    if (not maior or maior < v):
        maior = v
        vencedor = k
print("O vencedor é: {}, com o valor: {}".format(vencedor, maior))