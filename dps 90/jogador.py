import re

def imprimePartidaJogador(jogador): 
    for p in range(len(jogador["gols"])):
        print("Na partida {}, {} marcou {} gols".format(p + 1, jogador['nome'], jogador['gols'][p]))

def imprimeJogadores(jogadores: list[dict]):
    print(f"{'cod':<5}{'nome':<20}{'gols':<15}{'total':<5}")
    for i in range(len(jogadores)):
        gols = jogadores[i]['gols']
        print(f"{i:<5}{jogadores[i]['nome']:<20}{str(gols):<15}{jogadores[i]['total']:<15}")#{jogadores[i]['total']:<5}")

def soma(partidas):
    soma = 0
    for g in partidas:
        soma += g
    return soma

jogadores:list[dict] = []

while(True):
    nome = input("Digite um nome de jogador: ")
    qtd = int(input("Quantas partidas {} jogou? ".format(nome)))
    jogador = dict()
    jogador["nome"] = nome
    partidas = []
    for i in range(qtd):
        gols = int(input("Quantos gols na partida {}? ".format(i + 1)))
        partidas.append(gols)    
    jogador["gols"] = partidas
    jogador["total"] = soma(partidas)
    jogadores.append(jogador)
    cont = input("Continuar? (S/N) ").upper()
    while(not re.search("^[SN]{1}$", cont)):
        cont = input("DIGITA DIREITO ANIMAL! ")
    if(cont == 'N'):
        break

n = 0
imprimeJogadores(jogadores)
while(n != 99):
    n = int(input("Qual jogador mostrar? "))
    if(n == 99): break
    imprimePartidaJogador(jogadores[n])




