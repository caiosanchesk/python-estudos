def maior(*num):
    maior = None
    print("\nNa lista dos números: ")
    for v in num:
        print(f"{v}", end=' ')
        if(maior == None or maior < v): maior = v
    print("\nO maior é {}".format(maior))    

maior(2,4,5,6)
maior(12,41,55,6)
maior(54,1,5,7)
maior(22,3,8,63)