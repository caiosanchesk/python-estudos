def organiza(numeros, numero):
    numeros.append(numero)
    return numeros

numeros = []
pares = []
impares = []
qtd = int(input("Digite a quantidade de números: "))
for i in range(qtd):
    numero = int(input("Digite o número {}: ".format(i + 1)))
    numeros = organiza(numeros, numero)
for n in numeros:
    if(n % 2 == 0):
        pares = organiza(pares, n)
    else: 
        impares = organiza(impares, n)
print("A lista completa é: {}".format(numeros))
print("A lista de pares é: {}".format(pares))
print("A lista de impares é: {}".format(impares))