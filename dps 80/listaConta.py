qtd = 0
numeros = []
while(True):
    numero = int(input("Digite um número: "))
    numeros.append(numero)
    qtd += 1
    cont = input("Continuar: Sim(S) ou Não(qualquer valor)? ")
    if( not cont in 'Ss'):
        break
numeros.sort(reverse=True)
cinco = 'foi digitado' if 5 in numeros else 'não foi digitado'
print("Você digitou {} números\nVocê digitou esses números {} que estão ordenados de forma decrescente\nO número 5 {}".format(qtd, numeros, cinco))