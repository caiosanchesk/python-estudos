def imprimeMatriz(numeros: list[list]):
    for i in range(len(numeros)):
        for j in range(len(numeros[i])):
            print(f"[{numeros[i][j]:^5}]", end=" ")
        print()

def criaMatriz(tam):
    numeros:list[list] = []
    for i in range(0, tam):
        numeros.append([])
        for j in range(0, tam):
            numeros[i].append(int(input("Digite o número da posição ({},{}) da matriz: ".format(i, j))))
    return numeros

def buscaPares(numeros):
    soma_pares = 0
    for array in numeros:
        for n in array:
            if(n % 2 == 0): soma_pares += n
    return soma_pares

def buscaMaior(numeros):
    maior = None
    for n in numeros[1]:
        if(not maior or maior < n): maior = n
    return maior

def somaColuna(numeros):
    soma = 0
    for array in numeros:
        soma += array[2]
    return soma

tam = int(input("Digite o valor n para uma matriz n x n: "))
numeros = criaMatriz(tam)
imprimeMatriz(numeros)

soma_pares = buscaPares(numeros)
maior = buscaMaior(numeros)
soma_coluna = somaColuna(numeros)

print("="*20)
print("Dados\nA soma dos pares desta matriz é: {}".format(soma_pares))
print("A soma dos valores da terceira coluna desta matriz é: {}".format(soma_coluna))
print("O maior valor da segunda linha desta matriz é: {}".format(maior))