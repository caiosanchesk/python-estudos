from pessoa import Pessoa

def imprime(lista, tipo):
    print("As pessoas {} são: ".format(tipo))
    for i in lista:
        print(i.nome)

def ordena(pessoas: list[Pessoa]):
    for i in range(len(pessoas)):
        for p in range(len(pessoas)):
            if(pessoas[i].peso < pessoas[p].peso):
                aux = pessoas[i]
                pessoas[i] = pessoas[p]
                pessoas[p] = aux
    return pessoas

pessoas: list[Pessoa] = []
leves = []
pesados = []
qtd = int(input("Digite a quantidade de pessoas: "))
for i in range(qtd):
    nome = input("Digite o nome da {}ª pessoa: ".format(i + 1))
    peso = float(input("Digite o peso da {}ª pessoa: ".format(i + 1)))
    pessoa = Pessoa(nome, peso)
    pessoas.append(pessoa)

pessoas = ordena(pessoas)

tam = len(pessoas) 
iteracoes = int(tam / 2)
leves = []
for i in range(iteracoes): 
    leves = pessoas[:iteracoes]
    pesados = pessoas[iteracoes:]

imprime(leves, "leves")
imprime(pesados, "pesados")