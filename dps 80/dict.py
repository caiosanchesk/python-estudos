aluno = dict()
aluno['nome'] = input("Digite um nome: ")
aluno['media'] = int(input("Digite a média de {}: ".format(aluno['nome'])))
aluno['situacao'] = 'APROVADO' if aluno['media'] >= 7 else 'REPROVADO' if aluno['media'] < 3 else 'EM RECUPERAÇÃO'

print("{} está com média {} e está {}".format(aluno['nome'], aluno['media'], aluno['situacao']))


print("="*20)
for k, v in aluno.items(): # key, value in aluno
    print(f"{k} é igual a {v}")
print("="*20)