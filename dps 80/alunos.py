from aluno import Aluno
import re

def imprimeNotas(alunos):
    print("="*20+ " RELATÓRIO GERAL: "+"="*20)
    print(f"{'Código':^10}{'Aluno':<15}{'Média':>5}")
    for aluno in alunos:
        imprimeAluno(aluno)
    print("="*58)

def imprimeAluno(aluno):
    print(f"{aluno.numero:^10}{aluno.nome:<15}{media(aluno):>5}")

def media(aluno):
    return (aluno.nota1 + aluno.nota2) / 2

alunos = []
numero = 0
while(True):
    numero += 1 
    nome = input("Digite o nome do aluno: ")
    nota1 = float(input("Digite a nota 1 do aluno: "))
    nota2 = float(input("Digite a nota 2 do aluno: "))
    aluno = Aluno(numero, nome, nota1, nota2)
    alunos.append(aluno)
    cont = input("Continuar? S/N ").upper()
    while(not re.search("^[SN]$", cont)):
        cont = input("Continuar DIGITA CERTO!!!!? S/N ").upper()
    if(cont == 'N'): break

imprimeNotas(alunos)
n = 1
while(n != 99):
    n = int(input("Quer ver as notas de qual aluno? (99 para parar) "))
    if(n == 99): break 
    while(len(alunos) < n):
        n = int(input("DIGITA DIREITO SEU ANIMAL! "))
    aluno = alunos[n - 1]
    print("="*58)
    imprimeAluno(aluno)
    print("="*58)