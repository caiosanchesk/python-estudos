def cadastra(numeros: list[list], numero):
    if(numero % 2 == 0):
        numeros[0].append(numero)
    else: numeros[1].append(numero)
    return numeros

def ordena(numeros: list[list]):
    numeros[0].sort()
    numeros[1].sort()
    return numeros

numeros = [[],[]]
qtd = int(input('Digite a quantidade de números que quer cadastrar: '))
for i in range(qtd):
    numero = int(input("Digite o {}º número: ".format(i + 1)))
    cadastra(numeros, numero)
    
numeros = ordena(numeros)
print("Os números pares, ordenados são: {}".format(numeros[0]))
print("Os números ímpares, ordenados são: {}".format(numeros[1]))