import random as rd
import time

def sorteia():
    sorteio = []
    for i in range(6):
        sorteio.append(rd.randint(1, 60))
    return sorteio

def imprimeLista(lista):
    for i in range(len(lista)):
        time.sleep(0.8)
        print("Sorteio {}: {}".format(i + 1, lista[i]))

lista_sorteios = []
qtd_sorteios = int(input("Digite a quantidade de sorteios: "))
for i in range(qtd_sorteios):
    lista_sorteios.append(sorteia())
imprimeLista(lista_sorteios)